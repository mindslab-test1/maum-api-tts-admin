import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

buildscript {
    extra["springBootVersion"] = "2.3.3.RELEASE"
    extra["kotlinVersion"] = "1.4.10"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.10")
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${project.extra["springBootVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlinVersion"]}")
        classpath("org.jetbrains.kotlin:kotlin-allopen:${project.extra["kotlinVersion"]}")
    }
}

plugins {
    id("org.springframework.boot") version "2.3.3.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    id("com.google.protobuf") version "0.8.8"
    kotlin("jvm") version "1.4.10"
    kotlin("plugin.spring") version "1.4.10"
    kotlin("plugin.jpa") version "1.4.10"
    kotlin("plugin.serialization") version "1.4.10"
}

group = "ai.maum.tts"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val grpcVersion = "1.30.0"
val protocVersion = "3.13.0"

repositories {
    mavenCentral()
    google()
    maven(url = "http://maven.jahia.org/maven2")
}

sourceSets {
    main {
        java {
            srcDir("src/main/protoGen")
        }
    }
}

extra["springCloudVersion"] = "Hoxton.SR8"

dependencies {
    // spring
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.1.4.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    // grpc
    implementation("io.grpc:grpc-services")
    implementation("io.grpc:grpc-netty")
    implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")

    // spring third party
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // language
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // oracle
    implementation("com.oracle:ojdbc6:12.1.0.1")

    // kotlin serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.0")

    // test
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
}

protobuf {
    protoc { artifact = "com.google.protobuf:protoc:$protocVersion" }
    plugins {
        id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion" }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            //            task.builtins {
//                id("java") {
//                    outputSubDir = "protoGen"
//                }
//            }
            task.plugins {
                id("grpc") {
                    outputSubDir = "protoGen"
                }
            }
        }
    }
    generatedFilesBaseDir = "$projectDir/src/"
}

dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
