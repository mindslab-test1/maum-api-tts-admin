import requests


class ModelDelete:
    def __init__(
            self,
            admin_address: str = "localhost:8883",
            model_name: str = "test_model_name"
    ):
        self.admin_address = admin_address
        self.model_name = model_name

    def set_model(
            self,
            model_name: str = "test_model_name"
    ):
        self.model_name = model_name

    def send_request(self):
        payload = {
            "modelName": self.model_name
        }

        headers = {
            "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXNkZiJdLCJzY29wZSI6WyJzdHQiLCJ0dHMiLCJhdmF0YXIiXSwiZXhwIjoxNjA1MTYwMjkyLCJhdXRob3JpdGllcyI6WyJST0xFX0FQSSIsIlJPTEVfQU1MIiwiUk9MRV9TVEFUUyJdLCJqdGkiOiIxNTBiYzFjMS1jODFjLTRiNmUtODNjMS1lNjA3NmYyY2ZiZmUiLCJjbGllbnRfaWQiOiI2Y2Q1NWU5OS1hOWNjLTRjMDItYjhlMS00MjgwMGU3MGIwZjcifQ.NGspNDU5Z6SJrL17zC7lFra-PAiZiVCY049LIJgOmKIcrJyEzC71XBy7752j0UGZHNSzAWeUXYoSBB9AxbiEOOl0TZe9wDXhYbih_i5lrSo6wihA3ql8ddkfJQhxqVnCO7P9zMBkKNcHzGrPOkRFt2hwI6Td-fbMWhsmUdfgKxOLmZgqTI3KT4pUAZzLqCsF8V7LsDZhA9CYLvd6uTg-HOa0zSDOhlL7yuPZuHj7an9dg2j8rCa_gpKR2ChEKxfJGhOh9rujQsglXNhNHL-cIjWndblMVIVRjyRgdBPOIwSszOOLUH5Y7ug06eRYV0I_Qgr-QegHFA08rIuaoCoIsg",
            "Content-Type": "application/json"
        }

        response = requests.delete(
            "http://{}/tts/admin/api/model/delete".format(self.admin_address),
            headers=headers,
            json=payload
        )
        return response


if __name__ == '__main__':
    model_creater = ModelDelete(model_name="test")
    model_creater.send_request()
