import requests


class ModelCreater():
    def __init__(
            self,
            admin_address: str = "localhost:8883",
            model_name: str = "test_model_name",
            model_host: str = "127.0.0.1",
            model_port: int = 9999,
            model_samplerate: int = 22050,
            model_lang: str = "ko_KR",
            model_speaker: int = 0,
            model_open: bool = False,
            model_creator: str = "test_creator"
    ):
        self.admin_address = admin_address
        self.model_name = model_name
        self.model_host = model_host
        self.model_port = model_port
        self.model_samplerate = model_samplerate
        self.model_lang = model_lang
        self.model_speaker = model_speaker
        self.model_open = model_open
        self.model_creator = model_creator

    def set_model(
            self,
            model_name: str = "test",
            model_host: str = "127.0.0.1",
            model_port: int = 9999,
            model_samplerate: int = 22050,
            model_lang: str = "ko_KR",
            model_speaker: int = 0,
            model_open: bool = False,
            model_creator: str = "test_creator"
    ):
        self.model_name = model_name
        self.model_host = model_host
        self.model_port = model_port
        self.model_samplerate = model_samplerate
        self.model_lang = model_lang
        self.model_speaker = model_speaker
        self.model_open = model_open
        self.model_creator = model_creator

    def send_request(self):
        payload = {
            "modelName": self.model_name,
            "ttsModel": {
                "ip": self.model_host,
                "port": self.model_port,
                "sampleRate": self.model_samplerate,
                "speakerId": self.model_speaker,
                "lang": self.model_lang,
                "open": self.model_open
            },
            "creator": self.model_creator
        }

        headers = {
            "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXNkZiJdLCJzY29wZSI6WyJzdHQiLCJ0dHMiLCJhdmF0YXIiXSwiZXhwIjoxNjA1MTYwMjkyLCJhdXRob3JpdGllcyI6WyJST0xFX0FQSSIsIlJPTEVfQU1MIiwiUk9MRV9TVEFUUyJdLCJqdGkiOiIxNTBiYzFjMS1jODFjLTRiNmUtODNjMS1lNjA3NmYyY2ZiZmUiLCJjbGllbnRfaWQiOiI2Y2Q1NWU5OS1hOWNjLTRjMDItYjhlMS00MjgwMGU3MGIwZjcifQ.NGspNDU5Z6SJrL17zC7lFra-PAiZiVCY049LIJgOmKIcrJyEzC71XBy7752j0UGZHNSzAWeUXYoSBB9AxbiEOOl0TZe9wDXhYbih_i5lrSo6wihA3ql8ddkfJQhxqVnCO7P9zMBkKNcHzGrPOkRFt2hwI6Td-fbMWhsmUdfgKxOLmZgqTI3KT4pUAZzLqCsF8V7LsDZhA9CYLvd6uTg-HOa0zSDOhlL7yuPZuHj7an9dg2j8rCa_gpKR2ChEKxfJGhOh9rujQsglXNhNHL-cIjWndblMVIVRjyRgdBPOIwSszOOLUH5Y7ug06eRYV0I_Qgr-QegHFA08rIuaoCoIsg",
            "Content-Type": "application/json"
        }

        response = requests.put(
            "http://{}/tts/admin/api/model/create".format(self.admin_address),
            headers=headers,
            json=payload
        )
        return response


if __name__ == '__main__':
    model_creater = ModelCreater()
    model_creater.send_request()
