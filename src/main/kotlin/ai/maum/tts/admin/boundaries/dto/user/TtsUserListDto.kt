package ai.maum.tts.admin.boundaries.dto.user

import kotlinx.serialization.Serializable

@Serializable
data class TtsUserListDto(
        val modelName: String
)