package ai.maum.tts.admin.boundaries.controller

import ai.maum.tts.admin.boundaries.dto.ResponseDto
import ai.maum.tts.admin.boundaries.dto.user.TtsUserUpdateDto
import ai.maum.tts.admin.core.usecase.TtsAdminUseCase
import ai.maum.tts.admin.utils.enums.UserActions
import ai.maum.tts.admin.utils.types.BaseResponse
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("tts/admin/api/user")
class TtsUserCrudController (
        val ttsAdminUseCase: TtsAdminUseCase
){
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val successEntity = ResponseEntity.ok(ResponseDto())

    @PutMapping(
            "add"
    )
    fun userAddModel(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsUserUpdateDto
    ): BaseResponse{
        logger.debug("user add request")
        ttsAdminUseCase.executeUser(
                userDto = dto,
                action = UserActions.ADD
        )
        return successEntity
    }

    @PostMapping(
            "arrest"
    )
    fun userArrest(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsUserUpdateDto
    ): BaseResponse {
        logger.debug("user arrest")
        ttsAdminUseCase.executeUser(
                userDto = dto,
                action = UserActions.ARREST
        )
        return successEntity
    }

    @PostMapping(
            "release"
    )
    fun userRelease(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsUserUpdateDto
    ): BaseResponse {
        logger.debug("user release arrest")
        ttsAdminUseCase.executeUser(
                userDto = dto,
                action = UserActions.RELEASE
        )
        return successEntity
    }

    @DeleteMapping(
            "remove"
    )
    fun userRemoveModel(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsUserUpdateDto
    ): BaseResponse {
        logger.debug("user remove request")
        ttsAdminUseCase.executeUser(
                userDto = dto,
                action = UserActions.REMOVE
        )
        return successEntity
    }
}