package ai.maum.tts.admin.boundaries.controller

import ai.maum.tts.admin.boundaries.dto.ResponseDto
import ai.maum.tts.admin.core.usecase.TtsAdminUseCase
import ai.maum.tts.admin.utils.types.BaseResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(("tts/admin/api/init"))
class TtsApiInitController(
        val ttsAdminUseCase: TtsAdminUseCase
) {
    @GetMapping("getInitialData")
    fun initServer(@RequestHeader httpHeaders: HttpHeaders): BaseResponse{
        val responseDto = ResponseDto()
        responseDto.payload = ttsAdminUseCase.executeInit()
        return ResponseEntity.ok(responseDto)
    }
}