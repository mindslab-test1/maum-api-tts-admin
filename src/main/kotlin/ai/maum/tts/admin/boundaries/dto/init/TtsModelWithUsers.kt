package ai.maum.tts.admin.boundaries.dto.init

import ai.maum.tts.admin.core.model.TtsModel
import kotlinx.serialization.Serializable

@Serializable
data class TtsModelWithUsers(
        val model: TtsModel,
        val modelName: String,
        val users: MutableSet<String>
)