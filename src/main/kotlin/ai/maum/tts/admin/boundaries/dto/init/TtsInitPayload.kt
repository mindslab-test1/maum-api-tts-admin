package ai.maum.tts.admin.boundaries.dto.init

import ai.maum.tts.admin.boundaries.dto.AbstractPayload
import kotlinx.serialization.Serializable

@Serializable
data class TtsInitPayload(
        val modelList: MutableList<TtsModelWithUsers>
): AbstractPayload()