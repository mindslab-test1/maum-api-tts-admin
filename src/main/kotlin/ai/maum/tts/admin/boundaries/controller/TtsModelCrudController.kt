package ai.maum.tts.admin.boundaries.controller

import ai.maum.tts.admin.boundaries.dto.ResponseDto
import ai.maum.tts.admin.boundaries.dto.model.TtsModelCreateDto
import ai.maum.tts.admin.boundaries.dto.model.TtsModelDeleteDto
import ai.maum.tts.admin.boundaries.dto.model.TtsModelUpdateDto
import ai.maum.tts.admin.core.usecase.TtsAdminUseCase
import ai.maum.tts.admin.utils.enums.ModelActions
import ai.maum.tts.admin.utils.types.BaseResponse
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("tts/admin/api/model")
class TtsModelCrudController(
        val ttsAdminUseCase: TtsAdminUseCase
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val successEntity = ResponseEntity.ok(ResponseDto())

    @PutMapping(
            "/create"
    )
    fun modelCreate(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsModelCreateDto
    ): BaseResponse{
        logger.debug("model create request")
        ttsAdminUseCase.executeModel(dto, ModelActions.CREATE)
        return successEntity
    }

    @PostMapping(
            "/update"
    )
    fun modelUpdate(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsModelUpdateDto
    ): BaseResponse{
        logger.debug("model update request")
        ttsAdminUseCase.executeModel(dto, ModelActions.UPDATE)
        return successEntity
    }

    @DeleteMapping(
            "/delete"
    )
    fun modelDelete(
            @RequestHeader httpHeaders: HttpHeaders,
            @RequestBody dto: TtsModelDeleteDto
    ): BaseResponse{
        logger.debug("model delete request")
        ttsAdminUseCase.executeModel(dto, ModelActions.DELETE)
        return successEntity
    }
}