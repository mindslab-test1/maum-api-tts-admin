package ai.maum.tts.admin.boundaries.dto.user

import kotlinx.serialization.Serializable

@Serializable
data class TtsUserUpdateDto(
        val clientId: String,
        val modelName: String
)