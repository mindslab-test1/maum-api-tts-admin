package ai.maum.tts.admin.boundaries.dto.model

import ai.maum.tts.admin.core.model.TtsModel
import kotlinx.serialization.Serializable

@Serializable
data class TtsModelCreateDto(
        val ttsModel: TtsModel,
        val modelName: String,
        val creator: String
)