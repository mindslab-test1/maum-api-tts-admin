package ai.maum.tts.admin.boundaries.dto

import kotlinx.serialization.Serializable

@Serializable
data class ResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        var payload: AbstractPayload? = null,
)