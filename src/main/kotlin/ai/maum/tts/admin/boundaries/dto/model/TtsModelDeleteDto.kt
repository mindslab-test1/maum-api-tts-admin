package ai.maum.tts.admin.boundaries.dto.model

import kotlinx.serialization.Serializable

@Serializable
data class TtsModelDeleteDto(
        val modelName: String
)