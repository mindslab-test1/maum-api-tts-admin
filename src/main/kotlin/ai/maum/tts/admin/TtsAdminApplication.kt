package ai.maum.tts.admin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TtsAdminApplication

fun main(args: Array<String>) {
    runApplication<TtsAdminApplication>(*args)
}
