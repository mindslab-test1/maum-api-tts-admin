package ai.maum.tts.admin.infra.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore

@Configuration
@EnableResourceServer
class ResourceServerConfig(
    private val jwtSignKeyConfig: JwtSignKeyConfig
) : ResourceServerConfigurerAdapter() {

    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources
                .tokenStore(tokenStore())
                .resourceId(null) // Disabled since Microsoft does not support resource ids.
    }

    override fun configure(http: HttpSecurity) {
        http.headers().frameOptions().disable()
        http.authorizeRequests()
                .antMatchers("/tts/**")
                    .access("hasAuthority('ROLE_API') && #oauth2.hasScope('tts')")
                .anyRequest().authenticated()
    }

    @Bean
    fun tokenStore(): JwtTokenStore {
        return JwtTokenStore(jwtAccessTokenConverter())
    }

    @Bean
    fun jwtAccessTokenConverter(): JwtAccessTokenConverter {
        val accessTokenConverter = JwtAccessTokenConverter()
        accessTokenConverter.setVerifierKey(jwtSignKeyConfig.publicKey)
        return accessTokenConverter
    }
}