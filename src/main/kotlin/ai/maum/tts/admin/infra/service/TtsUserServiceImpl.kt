package ai.maum.tts.admin.infra.service

import ai.maum.tts.admin.core.model.TtsModelUpdate
import ai.maum.tts.admin.core.repository.TtsAdminRepository
import ai.maum.tts.admin.core.service.TtsUpdateService
import ai.maum.tts.admin.core.service.TtsUserService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TtsUserServiceImpl(
        val messageSender: TtsUpdateService,
        val ttsAdminRepository: TtsAdminRepository
): TtsUserService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun addUser(clientId: String, modelName: String) {
        logger.debug("add user to model request")
        ttsAdminRepository.addUserToModel(
                clientId = clientId,
                modelName = modelName
        )
        logger.debug("successfully updated database")
        val message = TtsModelUpdate(
                action = "update",
                modelName = modelName,
                client = clientId,
                clientAction = "add"
        )
        logger.debug("$message")
        messageSender.sendMessage(ttsModelUpdate = message)
    }

    override fun arrestUser(clientId: String, modelName: String) {
        logger.debug("arrest user from model request")
        ttsAdminRepository.arrestUserFromModel(
                clientId = clientId,
                modelName = modelName
        )
        logger.debug("successfully updated database")
        val message = TtsModelUpdate(
                action = "update",
                modelName = modelName,
                client = clientId,
                clientAction = "remove"
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }

    override fun releaseUser(clientId: String, modelName: String) {
        logger.debug("release user from arrested model request")
        ttsAdminRepository.releaseUser(
                clientId = clientId,
                modelName = modelName
        )
        logger.debug("successfully updated database")
        val message = TtsModelUpdate(
                action = "update",
                modelName = modelName,
                client = clientId,
                clientAction = "add"
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }

    override fun removeUser(clientId: String, modelName: String) {
        logger.debug("remove user from model request")
        ttsAdminRepository.removeUserFromModel(
                clientId = clientId,
                modelName = modelName
        )
        logger.debug("successfully updated database")
        val message = TtsModelUpdate(
                action = "update",
                modelName = modelName,
                client = clientId,
                clientAction = "remove"
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }
}