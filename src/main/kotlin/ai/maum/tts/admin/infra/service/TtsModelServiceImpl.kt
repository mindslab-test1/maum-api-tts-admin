package ai.maum.tts.admin.infra.service

import ai.maum.tts.admin.boundaries.dto.init.TtsModelWithUsers
import ai.maum.tts.admin.core.model.TtsModel
import ai.maum.tts.admin.core.model.TtsModelUpdate
import ai.maum.tts.admin.core.repository.TtsAdminRepository
import ai.maum.tts.admin.core.service.TtsModelService
import ai.maum.tts.admin.core.service.TtsUpdateService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TtsModelServiceImpl(
        val messageSender: TtsUpdateService,
        val ttsAdminRepository: TtsAdminRepository
): TtsModelService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun modelCreate(ttsModel: TtsModel, modelName: String, creator: String) {
        logger.debug("model create request")
        ttsAdminRepository.createModel(
                model = ttsModel,
                modelName = modelName,
                creator = creator
        )

        val message = TtsModelUpdate(
                action = "create",
                modelName = modelName,
                modelInfo = ttsModel,
                clientAction = "add",
                client = creator
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }

    override fun modelUpdate(modelName: String, ttsModel: TtsModel) {
        logger.debug("model update request")
        ttsAdminRepository.updateModel(
                model = ttsModel,
                modelName = modelName
        )

        val message = TtsModelUpdate(
                action = "update",
                modelName = modelName,
                modelInfo = ttsModel
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }

    override fun modelDelete(modelName: String) {
        logger.debug("model delete request")
        ttsAdminRepository.deleteModel(
                modelName = modelName
        )

        val message = TtsModelUpdate(
                action = "delete",
                modelName = modelName
        )
        logger.debug("$message")
        messageSender.sendMessage(message)
    }

    override fun getInitData(): MutableList<TtsModelWithUsers>{
        logger.debug("get init data")
        return ttsAdminRepository.getInitData()
    }
}