package ai.maum.tts.admin.infra.repository

import ai.maum.tts.admin.infra.entity.TtsModelEntity
import ai.maum.tts.admin.infra.entity.TtsUserEntity
import ai.maum.tts.admin.infra.entity.TtsValidEntity
import org.springframework.data.repository.CrudRepository

interface TtsValidRepository: CrudRepository<TtsValidEntity, Long> {
    fun findByClientAndModel(client: TtsUserEntity, model: TtsModelEntity): TtsValidEntity?
    fun findByClientAndModelAndActiveIsTrue(client: TtsUserEntity, model: TtsModelEntity): TtsValidEntity?
    fun findAllByModelAndActiveIsTrue(ttsModelEntity: TtsModelEntity): MutableList<TtsValidEntity>?
}