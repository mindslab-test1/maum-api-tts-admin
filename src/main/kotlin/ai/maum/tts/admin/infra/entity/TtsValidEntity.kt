package ai.maum.tts.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(
        name = "tts_authorized",
        uniqueConstraints = [
            UniqueConstraint(columnNames = ["client_id", "model_id"])
        ]
)
class TtsValidEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "TTS_AUTH_SEQ_GEN", sequenceName = "TTS_AUTH_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_AUTH_SEQ_GEN")
    var id: Long? = null

    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    var client: TtsUserEntity = TtsUserEntity()

    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    var model: TtsModelEntity = TtsModelEntity()

    @Column
    var arrested: Boolean = false
}