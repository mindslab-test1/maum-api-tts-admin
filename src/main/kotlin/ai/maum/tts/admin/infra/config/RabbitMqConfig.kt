package ai.maum.tts.admin.infra.config

import org.springframework.amqp.core.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RabbitMqConfig(
        @Value("\${spring.rabbitmq.exchange-name}")
        val exchangeName: String
) {
    @Bean
    fun fanout(): FanoutExchange{
        return FanoutExchange(exchangeName)
    }
}