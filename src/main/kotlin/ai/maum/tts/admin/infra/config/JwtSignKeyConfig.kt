package ai.maum.tts.admin.infra.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class JwtSignKeyConfig(
    @Value("\${jwt.public-key}")
    private val jwtPublicKey: String,
    @Value("\${jwt.sign-key:maum-api-auth-signkey}")
    private val jwtSignKey: String
) {

    val publicKey: String
        get() = jwtPublicKey
}