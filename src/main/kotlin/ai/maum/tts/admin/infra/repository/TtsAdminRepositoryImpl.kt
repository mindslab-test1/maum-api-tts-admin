package ai.maum.tts.admin.infra.repository

import ai.maum.tts.admin.boundaries.dto.init.TtsModelWithUsers
import ai.maum.tts.admin.core.model.TtsModel
import ai.maum.tts.admin.core.repository.TtsAdminRepository
import ai.maum.tts.admin.infra.entity.TtsModelEntity
import ai.maum.tts.admin.infra.entity.TtsUserEntity
import ai.maum.tts.admin.infra.entity.TtsValidEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository

@Repository
class TtsAdminRepositoryImpl(
        val ttsModelRepository: TtsModelRepository,
        val ttsUserRepository: TtsUserRepository,
        val ttsValidRepository: TtsValidRepository
): TtsAdminRepository{
    private val logger = LoggerFactory.getLogger(this.javaClass)
    override fun getModelInfo(modelName: String) {
        // TODO
        val result = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
    }

    override fun getModelUsers(modelName: String) {
        // TODO
        val result = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
    }

    override fun getInitData(): MutableList<TtsModelWithUsers> {
        val modelList = ttsModelRepository.findAllByActiveIsTrue() ?: throw Exception("TODO")
        val resultList = mutableListOf<TtsModelWithUsers>()

        modelList.forEach {modelIter ->
            val validList = ttsValidRepository.findAllByModelAndActiveIsTrue(modelIter) ?: mutableListOf()
            val userSet = mutableSetOf<String>()
            validList.forEach { validIter ->
                userSet.add(validIter.client.clientId)
            }

            resultList.add(TtsModelWithUsers(
                    model = TtsModel(
                            ip = modelIter.host,
                            port = modelIter.port.toInt(),
                            sampleRate = modelIter.sampleRate.toInt(),
                            speakerId = modelIter.speakerId.toInt(),
                            lang = modelIter.lang,
                            open = modelIter.open
                    ),
                    modelName = modelIter.modelName,
                    users = userSet
            ))
        }

        return resultList
    }

    override fun createModel(model: TtsModel, modelName: String, creator: String) {
        if(ttsModelRepository.existsByModelNameAndActiveIsTrue(modelName = modelName)) throw Exception("TODO")
        val creatorEntity = ttsUserRepository.findByClientIdAndActiveIsTrue(clientId = creator) ?: createUser(creator = creator)
        val ttsModelEntity = TtsModelEntity()
        ttsModelEntity.host = model.ip
        ttsModelEntity.port = model.port.toLong()
        ttsModelEntity.sampleRate = model.sampleRate!!.toLong()
        ttsModelEntity.speakerId = model.speakerId!!.toLong()
        ttsModelEntity.open = model.open!!
        ttsModelEntity.lang = model.lang!!

        ttsModelEntity.modelName = modelName
        val savedModelEntity = ttsModelRepository.save(ttsModelEntity)

        val ttsValidEntity = TtsValidEntity()
        ttsValidEntity.client = creatorEntity
        ttsValidEntity.model = savedModelEntity
        ttsValidRepository.save(ttsValidEntity)
    }

    override fun updateModel(model: TtsModel, modelName: String) {
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        ttsModelEntity.host = model.ip
        ttsModelEntity.port = model.port.toLong()
        ttsModelEntity.sampleRate = model.sampleRate!!.toLong()
        ttsModelEntity.speakerId = model.speakerId!!.toLong()
        ttsModelEntity.open = model.open!!
        ttsModelEntity.lang = model.lang!!

        ttsModelRepository.save(ttsModelEntity)
    }

    override fun deleteModel(modelName: String) {
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        ttsModelEntity.active = false

        ttsModelRepository.save(ttsModelEntity)
    }

    override fun addUserToModel(clientId: String, modelName: String) {
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        val ttsClientEntity = ttsUserRepository.findByClientIdAndActiveIsTrue(clientId = clientId) ?: createUser(creator = clientId)
        val ttsValidEntity = ttsValidRepository.findByClientAndModel(client = ttsClientEntity, model = ttsModelEntity) ?: TtsValidEntity()
        if(ttsValidEntity.active!!){
            ttsValidEntity.client = ttsClientEntity
            ttsValidEntity.model = ttsModelEntity
        } else {
            ttsValidEntity.active = true
        }

        ttsValidRepository.save(ttsValidEntity)
    }

    override fun arrestUserFromModel(clientId: String, modelName: String) {
        val ttsClientEntity = ttsUserRepository.findByClientIdAndActiveIsTrue(clientId = clientId) ?: throw Exception("TODO")
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        val ttsValidEntity = ttsValidRepository.findByClientAndModelAndActiveIsTrue(ttsClientEntity, ttsModelEntity) ?: throw Exception("TODO")
        ttsValidEntity.arrested = true

        ttsValidRepository.save(ttsValidEntity)
    }

    override fun releaseUser(clientId: String, modelName: String) {
        val ttsClientEntity = ttsUserRepository.findByClientIdAndActiveIsTrue(clientId = clientId) ?: throw Exception("TODO")
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        val ttsValidEntity = ttsValidRepository.findByClientAndModelAndActiveIsTrue(ttsClientEntity, ttsModelEntity) ?: throw Exception("TODO")
        ttsValidEntity.arrested = false

        ttsValidRepository.save(ttsValidEntity)
    }

    override fun removeUserFromModel(clientId: String, modelName: String) {
        val ttsClientEntity = ttsUserRepository.findByClientIdAndActiveIsTrue(clientId = clientId) ?: throw Exception("TODO")
        val ttsModelEntity = ttsModelRepository.findByModelNameAndActiveIsTrue(modelName = modelName) ?: throw Exception("TODO")
        val ttsValidEntity = ttsValidRepository.findByClientAndModelAndActiveIsTrue(ttsClientEntity, ttsModelEntity) ?: throw Exception("TODO")
        ttsValidEntity.active = false

        ttsValidRepository.save(ttsValidEntity)
    }

    private fun createUser(creator: String): TtsUserEntity{
        if(ttsUserRepository.existsByClientIdAndActiveIsTrue(clientId = creator)) throw Exception("TODO")
        val newClient = TtsUserEntity()
        newClient.clientId = creator
        return ttsUserRepository.save(newClient)
    }

}