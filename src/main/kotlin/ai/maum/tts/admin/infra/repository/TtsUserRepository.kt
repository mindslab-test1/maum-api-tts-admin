package ai.maum.tts.admin.infra.repository

import ai.maum.tts.admin.infra.entity.TtsUserEntity
import org.springframework.data.repository.CrudRepository

interface TtsUserRepository: CrudRepository<TtsUserEntity, Long> {
    fun findByClientIdAndActiveIsTrue(clientId: String): TtsUserEntity?

    fun existsByClientIdAndActiveIsTrue(clientId: String): Boolean
}