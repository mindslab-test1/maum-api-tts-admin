package ai.maum.tts.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*


@Entity
@DynamicUpdate
@Table(name = "tts_user")
class TtsUserEntity: BaseEntity() {
    @Id
    @SequenceGenerator(name = "TTS_USER_SEQ_GEN", sequenceName = "TTS_USER_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_USER_SEQ_GEN")
    var id: Long? = null

    @Column(unique = true)
    var clientId: String = ""
}