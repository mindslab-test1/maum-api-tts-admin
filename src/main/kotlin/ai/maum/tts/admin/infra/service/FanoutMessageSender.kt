package ai.maum.tts.admin.infra.service

import ai.maum.tts.admin.core.model.TtsModelUpdate
import ai.maum.tts.admin.core.service.TtsUpdateService
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.FanoutExchange
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service

@Service
class FanoutMessageSender(
        val template: RabbitTemplate,
        val fanout: FanoutExchange
): TtsUpdateService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun sendMessage(ttsModelUpdate: TtsModelUpdate) {
        logger.debug("send message inferred: ${Json.encodeToString(ttsModelUpdate)}")
        template.convertAndSend(
                fanout.name,
                "",
                Json.encodeToString(ttsModelUpdate)
        )
        logger.debug("sent message to all listeners")
    }
}