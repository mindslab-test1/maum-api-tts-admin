package ai.maum.tts.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "tts_model_info")
class TtsModelEntity : BaseEntity(){
    @Id
    @SequenceGenerator(name = "TTS_MODEL_SEQ_GEN", sequenceName = "TTS_MODEL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_MODEL_SEQ_GEN")
    var id: Long? = null

    @Column
    var modelName: String = ""

    @Column
    var host: String = ""

    @Column
    var port: Long = 0

    @Column
    var sampleRate: Long = 22050

    @Column
    var speakerId: Long = 0

    @Column
    var lang: String = "ko_KR"

    @Column
    var open: Boolean = false
}