package ai.maum.tts.admin.infra.repository

import ai.maum.tts.admin.infra.entity.TtsModelEntity
import org.springframework.data.repository.CrudRepository

interface TtsModelRepository: CrudRepository<TtsModelEntity, Long> {
    fun findByModelNameAndActiveIsTrue(modelName: String): TtsModelEntity?
    fun findAllByActiveIsTrue(): MutableList<TtsModelEntity>?

    fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}