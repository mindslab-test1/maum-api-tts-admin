package ai.maum.tts.admin.core.usecase

import ai.maum.tts.admin.boundaries.dto.init.TtsInitPayload
import ai.maum.tts.admin.boundaries.dto.model.TtsModelCreateDto
import ai.maum.tts.admin.boundaries.dto.model.TtsModelDeleteDto
import ai.maum.tts.admin.boundaries.dto.model.TtsModelUpdateDto
import ai.maum.tts.admin.boundaries.dto.user.TtsUserUpdateDto
import ai.maum.tts.admin.core.service.TtsModelService
import ai.maum.tts.admin.core.service.TtsUserService
import ai.maum.tts.admin.utils.enums.ModelActions
import ai.maum.tts.admin.utils.enums.UserActions
import org.springframework.stereotype.Component

@Component
class TtsAdminUseCase (
        val ttsModelService: TtsModelService,
        val ttsUserService: TtsUserService,
){
    fun executeModel(modelDto: Any, action: ModelActions){
        when(action){
            ModelActions.CREATE -> {
                modelDto as? TtsModelCreateDto ?: throw Exception("TODO")
                ttsModelService.modelCreate(
                        ttsModel = modelDto.ttsModel,
                        modelName = modelDto.modelName,
                        creator = modelDto.creator
                )
            }
            ModelActions.UPDATE -> {
                modelDto as? TtsModelUpdateDto ?: throw Exception("TODO")
                ttsModelService.modelUpdate(
                        modelName = modelDto.modelName,
                        ttsModel = modelDto.ttsModel
                )
            }
            ModelActions.DELETE -> {
                modelDto as? TtsModelDeleteDto ?: throw Exception("TODO")
                ttsModelService.modelDelete(
                        modelName = modelDto.modelName
                )
            }
        }
    }

    fun executeUser(userDto: Any, action: UserActions){
        userDto as? TtsUserUpdateDto ?: throw Exception("TODO")
        when(action){
            UserActions.ADD -> {
                ttsUserService.addUser(
                        clientId = userDto.clientId,
                        modelName = userDto.modelName
                )
            }
            UserActions.ARREST -> {
                ttsUserService.arrestUser(
                        clientId = userDto.clientId,
                        modelName = userDto.modelName
                )
            }
            UserActions.RELEASE ->{
                ttsUserService.releaseUser(
                        clientId = userDto.clientId,
                        modelName = userDto.modelName
                )
            }
            UserActions.REMOVE -> {
                ttsUserService.removeUser(
                        clientId = userDto.clientId,
                        modelName = userDto.modelName
                )
            }
        }
    }

    fun executeInit(): TtsInitPayload? {
        return TtsInitPayload(
                modelList = ttsModelService.getInitData()
        )
    }
}