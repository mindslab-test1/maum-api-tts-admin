package ai.maum.tts.admin.core.repository

import ai.maum.tts.admin.boundaries.dto.init.TtsModelWithUsers
import ai.maum.tts.admin.core.model.TtsModel

interface TtsAdminRepository {
    fun getModelInfo(modelName: String)
    fun getModelUsers(modelName: String)

    fun getInitData(): MutableList<TtsModelWithUsers>

    fun createModel(model: TtsModel, modelName: String, creator: String)
    fun updateModel(model: TtsModel, modelName: String)
    fun deleteModel(modelName: String)

    fun addUserToModel(clientId: String, modelName: String)
    fun arrestUserFromModel(clientId: String, modelName: String)
    fun releaseUser(clientId: String, modelName: String)
    fun removeUserFromModel(clientId: String, modelName: String)
}