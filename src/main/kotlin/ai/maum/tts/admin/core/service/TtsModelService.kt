package ai.maum.tts.admin.core.service

import ai.maum.tts.admin.boundaries.dto.init.TtsModelWithUsers
import ai.maum.tts.admin.core.model.TtsModel

interface TtsModelService {
    fun modelCreate(ttsModel: TtsModel, modelName: String, creator: String)
    fun modelUpdate(modelName: String, ttsModel: TtsModel)
    fun modelDelete(modelName: String)
    fun getInitData(): MutableList<TtsModelWithUsers>
}