package ai.maum.tts.admin.core.service

import ai.maum.tts.admin.core.model.TtsModelUpdate

interface TtsUpdateService {
    fun sendMessage(ttsModelUpdate: TtsModelUpdate)
}