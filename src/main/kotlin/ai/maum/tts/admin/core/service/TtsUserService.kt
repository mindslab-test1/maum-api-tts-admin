package ai.maum.tts.admin.core.service

interface TtsUserService {
    fun addUser(clientId: String, modelName: String)
    fun arrestUser(clientId: String, modelName: String)
    fun releaseUser(clientId: String, modelName: String)
    fun removeUser(clientId: String, modelName: String)
}