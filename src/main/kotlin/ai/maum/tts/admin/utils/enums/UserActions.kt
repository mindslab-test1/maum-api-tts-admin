package ai.maum.tts.admin.utils.enums

enum class UserActions {
    ADD,
    ARREST,
    RELEASE,
    REMOVE
}