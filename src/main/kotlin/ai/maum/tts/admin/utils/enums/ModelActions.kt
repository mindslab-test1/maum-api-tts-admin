package ai.maum.tts.admin.utils.enums

enum class ModelActions {
    CREATE,
    UPDATE,
    DELETE
}