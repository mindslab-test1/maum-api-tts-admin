package ai.maum.tts.admin.utils.types

import ai.maum.tts.admin.boundaries.dto.ResponseDto
import org.springframework.http.ResponseEntity

typealias BaseResponse = ResponseEntity<ResponseDto>